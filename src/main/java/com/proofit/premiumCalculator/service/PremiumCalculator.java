package com.proofit.premiumCalculator.service;

import com.proofit.premiumCalculator.exception.PremiumCalculationException;
import com.proofit.premiumCalculator.models.InsurancePolicy;
import com.proofit.premiumCalculator.models.InsuranceSubObject;
import com.proofit.premiumCalculator.models.RiskLevel;
import com.proofit.premiumCalculator.models.RiskType;
import com.proofit.premiumCalculator.repositories.CoefficientRepositoryStub;
import com.proofit.premiumCalculator.repositories.RiskCoefficientDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import static java.util.stream.Collectors.*;

@Service
public class PremiumCalculator {

    private final CoefficientRepositoryStub coefficientRepository;

    private static final int ROUNDING_SCALE = 2;

    @Autowired
    public PremiumCalculator(CoefficientRepositoryStub coefficientRepository) {
        this.coefficientRepository = coefficientRepository;
    }

    public BigDecimal calculate(InsurancePolicy policy) throws PremiumCalculationException {
        List<InsuranceSubObject> subObjects = policy.getObjects().stream()
                .flatMap(obj -> obj.getSubObjects().stream())
                .collect(toList());
        if (subObjects.isEmpty()) {
            throw new PremiumCalculationException(String.format("Objects to insure not found for policy %s", policy.getPolicyNumber()));
        }

        BigDecimal premiumAmount = BigDecimal.ZERO;
        Set<RiskType> risks = getRiskTypes(subObjects);
        Map<RiskType, RiskCoefficientDefinition> coefficients = coefficientRepository.getPolicyCoefficients(policy.getPolicyNumber(), risks)
                .stream()
                .collect(toMap(RiskCoefficientDefinition::getRiskType, Function.identity()));

        for (RiskType riskType : risks) {
            RiskCoefficientDefinition riskDefinition = coefficients.get(riskType);
            BigDecimal sumInsured = getInsuredSumByRiskType(riskType, subObjects);
            RiskLevel riskLevel = riskType.defineRiskLevel(riskDefinition.getCoefficientApplyingThreshold(), sumInsured);
            BigDecimal premiumCoefficient = riskDefinition.getCoefficients().get(riskLevel);

            premiumAmount = premiumAmount.add(sumInsured.multiply(premiumCoefficient));
        }
        return premiumAmount.setScale(ROUNDING_SCALE, RoundingMode.HALF_UP);
    }

    private BigDecimal getInsuredSumByRiskType(RiskType riskType, List<InsuranceSubObject> subObjects) {
        return subObjects.stream()
                .filter(subObject -> subObject.getRiskType() == riskType)
                .map(InsuranceSubObject::getSumInsured)
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }

    private Set<RiskType> getRiskTypes(List<InsuranceSubObject> subObjects) {
        return subObjects.stream().map(InsuranceSubObject::getRiskType).collect(toSet());
    }
}
