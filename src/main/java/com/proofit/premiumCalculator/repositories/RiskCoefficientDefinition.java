package com.proofit.premiumCalculator.repositories;

import com.proofit.premiumCalculator.models.RiskLevel;
import com.proofit.premiumCalculator.models.RiskType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class RiskCoefficientDefinition {
    private RiskType riskType;
    private BigDecimal coefficientApplyingThreshold;
    private Map<RiskLevel, BigDecimal> coefficients;

    private static RiskCoefficientDefinition create(RiskType riskType, BigDecimal coefficientApplyingThreshold, BigDecimal defaultCoefficient, BigDecimal higherCoefficient) {
        Map<RiskLevel, BigDecimal> coefficients = new HashMap<>();
        coefficients.put(RiskLevel.DEFAULT, defaultCoefficient);
        coefficients.put(RiskLevel.HIGH, higherCoefficient);
        return new RiskCoefficientDefinition(riskType, coefficientApplyingThreshold, coefficients);
    }

    public static RiskCoefficientDefinition fire(BigDecimal coefficientApplyingThreshold, BigDecimal standardCoefficient, BigDecimal higherCoefficient) {
        return create(RiskType.FIRE, coefficientApplyingThreshold, standardCoefficient, higherCoefficient);
    }

    public static RiskCoefficientDefinition theft(BigDecimal coefficientApplyingThreshold, BigDecimal standardCoefficient, BigDecimal higherCoefficient) {
        return create(RiskType.THEFT, coefficientApplyingThreshold, standardCoefficient, higherCoefficient);
    }
}
