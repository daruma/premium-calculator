package com.proofit.premiumCalculator.repositories;

import com.proofit.premiumCalculator.models.RiskType;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import static java.util.Arrays.asList;

@Repository
public class CoefficientRepositoryStub {

    // DB query to obtain RiskCoefficientDefinitions for a policy
    public List<RiskCoefficientDefinition> getPolicyCoefficients(String policyNumber, Set<RiskType> riskTypes) {

        // select definitions from definitions where policyNumber = policyNumber and riskType in (riskTypes)
        return asList(RiskCoefficientDefinition.fire(BigDecimal.valueOf(100), BigDecimal.valueOf(0.014), BigDecimal.valueOf(0.024)),
                RiskCoefficientDefinition.theft(BigDecimal.valueOf(15), BigDecimal.valueOf(0.11), BigDecimal.valueOf(0.05)));
    }
}
