package com.proofit.premiumCalculator.controller;

import com.proofit.premiumCalculator.exception.InconsistentPolicyDataException;
import com.proofit.premiumCalculator.exception.PremiumCalculationException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@Slf4j
public class ControllerExceptionHandler {

    @ExceptionHandler({PremiumCalculationException.class, IllegalStateException.class})
    public ResponseEntity<ErrorResponse> handleException(Exception ex, WebRequest request) {
        log.error(ex.getMessage(), ex);
        log.info(request.toString());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse("Failed to calculate premium."));
    }

    @ExceptionHandler(InconsistentPolicyDataException.class)
    public ResponseEntity<ErrorResponse> handleBadDataException(InconsistentPolicyDataException ex, WebRequest request) {
        log.error(ex.getMessage(), ex);
        log.info(request.toString());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(ex.getMessage()));
    }

    @ResponseStatus(reason = "Unexpected error occurred")
    @ExceptionHandler(Exception.class)
    public void handleUnexpectedException(Exception ex) {
        log.error(ex.getMessage(), ex);
    }

    @AllArgsConstructor
    @Getter
    private static class ErrorResponse {
        private final String errorMessage;
    }

}
