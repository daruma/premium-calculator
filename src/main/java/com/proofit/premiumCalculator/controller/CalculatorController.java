package com.proofit.premiumCalculator.controller;

import com.proofit.premiumCalculator.exception.InconsistentPolicyDataException;
import com.proofit.premiumCalculator.exception.PremiumCalculationException;
import com.proofit.premiumCalculator.models.InsurancePolicy;
import com.proofit.premiumCalculator.models.PolicyStatus;
import com.proofit.premiumCalculator.service.PremiumCalculator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
@RequestMapping("/api/v1/pnc/policy")
@Slf4j
public class CalculatorController {

    private final PremiumCalculator premiumCalculator;

    @Autowired
    public CalculatorController(PremiumCalculator premiumCalculator) {
        this.premiumCalculator = premiumCalculator;
    }

    @GetMapping(path = "/echo", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> echo() {
        return ResponseEntity.ok("I am OK!");
    }

    @PostMapping(path = "/premium", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BigDecimal> calculatePolicyPremium(@RequestBody InsurancePolicy policy) throws PremiumCalculationException {
        validatePolicy(policy);

        return ResponseEntity.ok(premiumCalculator.calculate(policy));
    }

    private void validatePolicy(InsurancePolicy policy) throws InconsistentPolicyDataException {
        if (PolicyStatus.APPROVED != policy.getPolicyStatus()) {
            throw new InconsistentPolicyDataException(String.format("Policy '%s' must be approved.", policy.getPolicyNumber()));
        }
        if (policy.getObjects() == null || policy.getObjects().isEmpty()) {
            throw new InconsistentPolicyDataException(String.format("Policy '%s' must have one or more insurable objects", policy.getPolicyNumber()));
        }
    }
}
