package com.proofit.premiumCalculator.exception;

public class PremiumCalculationException extends Exception {
    public PremiumCalculationException(String message) {
        super(message);
    }
}
