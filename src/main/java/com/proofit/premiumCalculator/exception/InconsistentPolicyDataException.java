package com.proofit.premiumCalculator.exception;

public class InconsistentPolicyDataException extends PremiumCalculationException {
    public InconsistentPolicyDataException(String message) {
        super(message);
    }
}
