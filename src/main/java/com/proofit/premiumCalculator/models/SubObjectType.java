package com.proofit.premiumCalculator.models;

public enum SubObjectType {
    TV_SET, MOBILE_PHONE, DESKTOP_COMPUTER, LAPTOP_COMPUTER, SOUND_SYSTEM, MICROWAVE_OVEN,
}
