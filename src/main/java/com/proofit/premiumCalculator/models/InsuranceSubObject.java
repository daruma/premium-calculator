package com.proofit.premiumCalculator.models;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;

@Builder
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class InsuranceSubObject {
    private final SubObjectType type;
    private final BigDecimal sumInsured;
    private final RiskType riskType;
}
