package com.proofit.premiumCalculator.models;

public enum ObjectType {
    APARTMENTS, HOUSE, CAR, MOTORBIKE, BOAT, YACHT
}
