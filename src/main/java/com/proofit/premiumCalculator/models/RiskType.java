package com.proofit.premiumCalculator.models;

import java.math.BigDecimal;

public enum RiskType {
    FIRE {
        @Override
        public RiskLevel defineRiskLevel(BigDecimal threshold, BigDecimal sumInsured) {
            return sumInsured.compareTo(threshold) > 0 ? RiskLevel.HIGH : RiskLevel.DEFAULT;
        }
    },
    THEFT {
        @Override
        public RiskLevel defineRiskLevel(BigDecimal threshold, BigDecimal sumInsured) {
            return sumInsured.compareTo(threshold) >= 0 ? RiskLevel.HIGH : RiskLevel.DEFAULT;
        }
    };

    public abstract RiskLevel defineRiskLevel(BigDecimal threshold, BigDecimal sumInsured);

}

