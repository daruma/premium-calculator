package com.proofit.premiumCalculator.models;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class InsurancePolicy {
    private final String policyNumber;
    private final PolicyStatus policyStatus;
    private final List<InsuranceObject> objects;
}
