package com.proofit.premiumCalculator.models;

public enum PolicyStatus {
    PENDING, REGISTERED, APPROVED, EXTENDED, EXHAUSTED
}
