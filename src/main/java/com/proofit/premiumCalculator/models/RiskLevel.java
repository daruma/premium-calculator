package com.proofit.premiumCalculator.models;

public enum RiskLevel {
    DEFAULT, HIGH
}
