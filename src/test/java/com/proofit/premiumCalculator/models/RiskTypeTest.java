package com.proofit.premiumCalculator.models;


import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


public class RiskTypeTest {

    @Test
    public void defineRiskLevelFireHigh() {
        assertThat(RiskType.FIRE.defineRiskLevel(BigDecimal.valueOf(100), BigDecimal.valueOf(100.01)), is(RiskLevel.HIGH));
    }

    @Test
    public void defineRiskLevelFireDefault() {
        assertThat(RiskType.FIRE.defineRiskLevel(BigDecimal.valueOf(100), BigDecimal.valueOf(100)), is(RiskLevel.DEFAULT));
    }

    @Test
    public void defineRiskLevelTheftHigh() {
        assertThat(RiskType.THEFT.defineRiskLevel(BigDecimal.valueOf(15), BigDecimal.valueOf(15)), is(RiskLevel.HIGH));
    }

    @Test
    public void defineRiskLevelTheftDefault() {
        assertThat(RiskType.THEFT.defineRiskLevel(BigDecimal.valueOf(15), BigDecimal.valueOf(14.99)), is(RiskLevel.DEFAULT));
    }
}