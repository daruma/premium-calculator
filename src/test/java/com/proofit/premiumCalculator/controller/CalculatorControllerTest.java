package com.proofit.premiumCalculator.controller;

import com.proofit.premiumCalculator.repositories.CoefficientRepositoryStub;
import com.proofit.premiumCalculator.service.PremiumCalculator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest({CalculatorController.class, PremiumCalculator.class, CoefficientRepositoryStub.class})
public class CalculatorControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    @SuppressWarnings("ConstantConditions")
    public void calculatePolicyPremium() throws Exception {
        try (InputStream is = this.getClass().getClassLoader().getResourceAsStream("post.json");
             BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
            String load = reader.lines().reduce("", String::concat);

            MvcResult result = mockMvc.perform(post("/api/v1/pnc/policy/premium")
                    .accept(MediaType.APPLICATION_JSON_VALUE)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .content(load))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                    .andReturn();

            assertThat(result.getResponse().getContentAsString(), is("6.27"));
        }
    }

    @Test
    @SuppressWarnings("ConstantConditions")
    public void calculatePolicyPremiumThrowsException() throws Exception {
        try (InputStream is = this.getClass().getClassLoader().getResourceAsStream("postWithException.json");
             BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
            String load = reader.lines().reduce("", String::concat);

            MvcResult result = mockMvc.perform(post("/api/v1/pnc/policy/premium")
                    .accept(MediaType.APPLICATION_JSON_VALUE)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .content(load))
                    .andExpect(status().is(400))
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(jsonPath("$.errorMessage").value("Policy 'LV000500050005' must be approved."))
                    .andReturn();
        }
    }
}