package com.proofit.premiumCalculator.service;

import com.proofit.premiumCalculator.exception.PremiumCalculationException;
import com.proofit.premiumCalculator.models.InsuranceObject;
import com.proofit.premiumCalculator.models.InsurancePolicy;
import com.proofit.premiumCalculator.models.InsuranceSubObject;
import com.proofit.premiumCalculator.models.RiskType;
import com.proofit.premiumCalculator.repositories.CoefficientRepositoryStub;
import com.proofit.premiumCalculator.repositories.RiskCoefficientDefinition;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.nCopies;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PremiumCalculatorTest {

    @InjectMocks
    private PremiumCalculator calculator;
    @Mock
    private CoefficientRepositoryStub repository;

    @Test
    public void calculateFireStandardTheftHigh_5_Objects() throws PremiumCalculationException {
        when(repository.getPolicyCoefficients(anyString(), any())).thenReturn(getPolicyCoefficients());

        InsuranceSubObject fire = mock(InsuranceSubObject.class);
        when(fire.getRiskType()).thenReturn(RiskType.FIRE);
        when(fire.getSumInsured()).thenReturn(BigDecimal.valueOf(9.99)).thenReturn(BigDecimal.valueOf(20)).thenReturn(BigDecimal.valueOf(70));
        InsuranceSubObject theft = mock(InsuranceSubObject.class);
        when(theft.getSumInsured()).thenReturn(BigDecimal.valueOf(10.01)).thenReturn(BigDecimal.valueOf(20)).thenReturn(BigDecimal.valueOf(70));
        when(theft.getRiskType()).thenReturn(RiskType.THEFT);

        InsurancePolicy insurancePolicy = mock(InsurancePolicy.class);
        when(insurancePolicy.getPolicyNumber()).thenReturn("LV-0050050005");
        InsuranceObject object = mock(InsuranceObject.class);
        when(object.getSubObjects()).thenReturn(nCopies(3, theft)).thenReturn(nCopies(3, fire));
        when(insurancePolicy.getObjects()).thenReturn(nCopies(2, object));

        assertThat(calculator.calculate(insurancePolicy), is(new BigDecimal("6.40")));

        verify(repository).getPolicyCoefficients(anyString(), any());
    }

    @Test
    public void calculateFireHigh_1_Object() throws PremiumCalculationException {
        when(repository.getPolicyCoefficients(anyString(), any())).thenReturn(getPolicyCoefficients());

        InsuranceSubObject subObject = mock(InsuranceSubObject.class);
        when(subObject.getSumInsured()).thenReturn(BigDecimal.valueOf(100.01));
        when(subObject.getRiskType()).thenReturn(RiskType.FIRE);

        InsurancePolicy insurancePolicy = mock(InsurancePolicy.class);
        when(insurancePolicy.getPolicyNumber()).thenReturn("LV-0050050005");
        InsuranceObject object = mock(InsuranceObject.class);
        when(object.getSubObjects()).thenReturn(nCopies(1, subObject));
        when(insurancePolicy.getObjects()).thenReturn(nCopies(1, object));

        assertThat(calculator.calculate(insurancePolicy), is(new BigDecimal("2.40")));
        verify(repository).getPolicyCoefficients(anyString(), any());
    }

    @Test(expected = PremiumCalculationException.class)
    public void calculateFireHighThrowsException() throws PremiumCalculationException {
        InsurancePolicy insurancePolicy = mock(InsurancePolicy.class);
        InsuranceObject object = mock(InsuranceObject.class);
        when(object.getSubObjects()).thenReturn(emptyList());
        when(insurancePolicy.getObjects()).thenReturn(nCopies(1, object));

        calculator.calculate(insurancePolicy);
    }

    private List<RiskCoefficientDefinition> getPolicyCoefficients() {
        return asList(RiskCoefficientDefinition.fire(BigDecimal.valueOf(100), BigDecimal.valueOf(0.014), BigDecimal.valueOf(0.024)),
                RiskCoefficientDefinition.theft(BigDecimal.valueOf(15), BigDecimal.valueOf(0.11), BigDecimal.valueOf(0.05)));
    }
}