# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Description ###

* A test project for calculating insurance premium on a PnC policy
* As a result ones obtains a premium value to be paid for an insurance policy 
* Contains of: rest controller, calculation service, repository stub,
domain models, no GUI (according to reqs)
* Service, controller and one enum (contains logic) covered by tests 

### Ho to run the projects ###

In order to run the project your system should be configured accordingly:
* Java 1.8 or higher
* Maven
* no DB used in this project

The project is a Spring Boot project, easily runnable, there is no need any special effort to get it up and running from your preferred IDE.
Once you have it running, use you preferred tool for making and sending http request.
(Please see [pre-requisites.md]() file for more instructions).

