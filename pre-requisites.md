### POST ###
Rest api controller has one post method.
It may be queried following the path 
[localhost:8080/api/v1/pnc/policy/premium](http://localhost:8080/api/v1/pnc/policy/premium) and providing with a payload, e.g.

{
  "policyNumber": "LV000500050005",
  "policyStatus": "APPROVED",
  "objects": [
    {
      "objectType": "HOUSE",
      "subObjects": [
        {
          "type": "TV_SET",
          "sumInsured": 40.99,
          "riskType": "FIRE"
        },{
          "type": "TV_SET",
          "sumInsured": 99.99,
          "riskType": "THEFT"
        },{
          "type": "TV_SET",
          "sumInsured": 50.0,
          "riskType": "FIRE"
        }
      ]
    }
  ]
}

### GET ###
It is also available to make a get-echo request to make sure the project is launched:

[localhost:8080/api/v1/pnc/policy/echo](http://localhost:8080/api/v1/pnc/policy/echo)